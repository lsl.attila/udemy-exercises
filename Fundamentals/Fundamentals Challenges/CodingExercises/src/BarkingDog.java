public class BarkingDog {

    public static void main(String[] args) {
        boolean wakeUp = shouldWakeUp(true, -1);
        System.out.println(wakeUp);
    }

    public static boolean shouldWakeUp(boolean dogIsBarking, int hourOfDay) {
        if (hourOfDay < 0 && hourOfDay > 23) {
            return false;
        }
        if ((dogIsBarking == true) && ((hourOfDay < 8 && hourOfDay >= 0) || (hourOfDay > 22 && hourOfDay < 24))) {
            return true;
        }
        return false;

    }
}
