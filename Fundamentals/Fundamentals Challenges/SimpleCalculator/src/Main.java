import carpetCostCalculator.Calculator;
import carpetCostCalculator.Carpet;
import carpetCostCalculator.Floor;

public class Main {
    public static void main(String[] args) {

//        SimpleCalculator calculator = new SimpleCalculator();
//        calculator.setFirstNumber(5);
//        calculator.setSecondNumber(4);
//        System.out.println("add = " + calculator.getAdditionResult());
//        System.out.println("subtract = " + calculator.getSubtractionResult());
//        calculator.setFirstNumber(5.25);
//        calculator.setSecondNumber(0);
//        System.out.println("multiply = " + calculator.getMultiplicationResult());
//        System.out.println("divide = " + calculator.getDivisionResult());

//        Person person = new Person();
//        person.setFirstName("");
//        person.setLastName("");
//        person.setAge(10);
//        System.out.println("fullName= " + person.getFullName());
//        System.out.println("teen= " + person.isTeen());
//        person.setFirstName("John");
//        person.setAge(18);
//        System.out.println("fullName= " + person.getFullName());
//        System.out.println("teen= " + person.isTeen());
//        person.setLastName("Smith");
//        System.out.println("fullName= " + person.getFullName());

//        Account bobsAccount=new Account("12345",0.00,"Bob Brown","bobby@yahoo.com","1254897");
//        bobsAccount.setNumber("12345");
//        bobsAccount.setBalance(0.00);
//        bobsAccount.setCustomerName("Bob Brown");
//        bobsAccount.setCustomerEmailAddress("myemail@bob.com");
//        bobsAccount.setCustomerPhoneNumber("1325468546");

//        bobsAccount.withdrawal(100.0);
//
//        bobsAccount.deposit(50.0);
//        bobsAccount.withdrawal(100.0);
//
//        bobsAccount.deposit(51.0);
//        bobsAccount.withdrawal(100.0);
//
//        Account timsAccount=new Account("Tim","tim@email.com","1234");
//        System.out.println(timsAccount.getNumber()+" name "+timsAccount.getCustomerName());

//        VipPerson person1=new VipPerson();
//        System.out.println(person1.getName());
//
//        VipPerson person2=new VipPerson("Bob",25000);
//        System.out.println(person2.getName());
//
//        VipPerson person3=new VipPerson("Tim",100,"tim@yahoo.com");
//        System.out.println(person3.getName());
//        System.out.println(person3.getEmailAddress());

//        Wall wall=new Wall(5,4);
//        System.out.println("area= "+wall.getArea());
//
//        wall.setHeight(-1.5);
//        System.out.println("width= "+wall.getWidth());
//        System.out.println("height= "+wall.getHeight());
//        System.out.println("area= "+wall.getArea());

//        Point first = new Point(6, 5);
//        Point second = new Point(3, 1);
//        System.out.println("distance(0,0)= " + first.distance());
//        System.out.println("distance(second)= " + first.distance(second));
//        System.out.println("distance(2,2)= " + first.distance(2, 2));
//        Point point = new Point();
//        System.out.println("distance()= " + point.distance());

        Carpet carpet=new Carpet(3.5);
        Floor floor=new Floor(2.75,4.0);
        Calculator calculator=new Calculator(floor,carpet);
        System.out.println("total= "+calculator.getTotalCost());
        carpet=new Carpet(1.5);
        floor=new Floor(5.4,4.5);
        calculator=new Calculator(floor,carpet);
        System.out.println("total= "+calculator.getTotalCost());

    }
}
