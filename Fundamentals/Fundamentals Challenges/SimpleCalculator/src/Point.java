public class Point {

    private int x;
    private int y;

    public Point(){

    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance(){
        double result=Math.sqrt((0-x)*(0-x)+(0-y)*(0-y));
        return result;
    }

    public double distance(int n, int m){
        double result=Math.sqrt((n-x)*(n-x)+(m-y)*(m-y));
        return result;
    }

   public double distance(Point point){
       int a=3;
       int b=4;
       double result=Math.sqrt((a-x)*(a-x)+(b-y)*(b-y));
       return result;

   }
}
