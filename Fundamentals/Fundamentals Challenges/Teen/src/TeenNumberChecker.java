public class TeenNumberChecker {

    public static boolean hasTeen(int teenOne, int teenTwo , int teenThree){
        if((teenOne>=13&&teenOne<=19)||(teenTwo>=13&&teenTwo<=19)||(teenThree>=13&&teenThree<=19)){
            return true;
        }else {
            return false;
        }
    }

    public static boolean isTeen(int teenFour){
        if(teenFour>=13&&teenFour<=19){
            return true;
        }else {
            return false;
        }
    }
}
