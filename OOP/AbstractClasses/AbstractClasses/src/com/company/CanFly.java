package com.company;

public interface CanFly {
    void fly();
    //all methods of an interface are public(abstract classes can have private methods), an interface cannot have a constructor while an abstract class can have
}
