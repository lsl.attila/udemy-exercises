package com.company;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Yorkie");
        dog.breathe();
        dog.eat();

        Parrot parrot = new Parrot("Australian ringneck");
        parrot.breathe();
        parrot.eat();
        parrot.fly();

        Penguin penguin = new Penguin("Emperor");
        penguin.fly();
    }
}

//abstract classes - cannot instantiate them, you can declare fields that are not static and final and define public, protected, and private concrete methods
//an abstract class can extend only one parent class but it can implement multiple interfaces
//when an abstract class is subclasssed, the subclass usually provides implementations for all of the abstract methods in its parent class
//however if not, then the subclass must also be declared abstract
//the purpose of an abstract class is to provide a common definition of a base class that multiple derived classes can share

//interfaces - is just a declaration of methods of a Class, its not the implementation
//in an interface we define what kind of operation an object can perform
//interfaces form a contract between the class and the outside world and this contract is enforced at build time by the compiler
//you cannot instantiate them, all methods in interfaces are automatically public and abstract
//an interface can extend another interface but cannot implement one
//since java 8 interfaces can contain default methods and since java 9 they can also contain private methods

