package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // For linked lists java allocates 4 bytes of memory, so a linked list has an index, address and value
        Customer customer = new Customer("Tim", 55.96);
        Customer anotherCustomer;//instance
        anotherCustomer = customer;//anotherCustomer point to customer, Java is saving the memory
        anotherCustomer.setBalance(12.18);
        System.out.println("Balance for customer " + customer.getName() + " is " + customer.getBalance());

        ArrayList<Integer> intList = new ArrayList<Integer>();

        intList.add(1);
        intList.add(3);
        intList.add(4);

        for (int i = 0; i < intList.size(); i++) {
            System.out.println(i + ": " + intList.get(i));
        }

        intList.add(1, 2);

        for (int i = 0; i < intList.size(); i++) {
            System.out.println(i + ": " + intList.get(i));
        }
    }
}
