package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] myArray = new int[5];
        int[] myanotherArray = myArray;

        System.out.println("myArray " + Arrays.toString(myArray));
        System.out.println("myAnotherArray " + Arrays.toString(myanotherArray));

        myanotherArray=new int[]{4,5,6,7,8};
        modifyArray(myArray);
        //if you change one value from on of the arrays automatically changes in the other too, just in this case because myAnotherArray=myArray

        System.out.println("myArray " + Arrays.toString(myArray));
        System.out.println("myAnotherArray " + Arrays.toString(myanotherArray));

        modifyArray(myArray);

        System.out.println("after modify myArray " + Arrays.toString(myArray));
        System.out.println("after modify myAnotherArray " + Arrays.toString(myanotherArray));


    }

    private static void modifyArray(int[] array) {
        array[0] = 2;
        array = new int[]{1, 2, 3, 4, 5};
    }
}
