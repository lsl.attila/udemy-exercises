package com.company;

public class Main {

    public static void main(String[] args) {
        ITelephone attisPhone;
        attisPhone = new DeskPhone(123456);
        attisPhone.powerOn();
        attisPhone.callPhone(123456);
        attisPhone.answer();

        attisPhone = new MobilePhone(65489);
        attisPhone.powerOn();
        attisPhone.callPhone(98797);
        attisPhone.answer();
    }
}
