package com.company;

@FunctionalInterface
interface Cab {//When an interface will have exactly 1 abstract method we can declare it as a Functional Interface

    //        void bookCab(); // -> by default public abstract void bookCab();
//        void bookCab(String source, String destination); // -> by default public abstract void bookCab();
    double bookCab(String source, String destination);
}


//    class UberX implements Cab{
//        public void bookCab(){
//            System.out.println("UberX Booked!! Arriving Soon!");
//        }
//    }


public class Main {

    int instanceVar = 10;
    static int sVar = 100;

    public static void main(String[] args) {

//        1.)
//        Cab cab = new UberX();
//        cab.bookCab();

//      2.)
        //Anonymous Class implementation
//        Cab cab = new Cab() {
//            @Override
//            public void bookCab() {
//                System.out.println("UberX booked, arriving soon!");
//            }
//        };
//        cab.bookCab();

//        3.) Using lambda expression

//        double fare = cab.bookCab("Centru", "Feleac");
//        System.out.println("fare shall be: "+fare);

//        lambda expressions can take zero, one or multiple parameters
//         zero parameters: () -> sout("zero parameter lambda")
//         one parameter: (param) -> sout("one parameter: "+param);
//         multiple parameters (p1,p2) -> sout("multiple parameters: "+p1+" "+p2);

    }

    Cab cab = (source, destination) -> {
        System.out.println("Uberx booked from "+source+" to "+destination+" ,arriving soon");
        System.out.println("instanceVar is: "+instanceVar);
        System.out.println("static int variable is: "+Main.sVar);
        return 850.12;
    };

}
